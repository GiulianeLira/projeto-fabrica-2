package br.com.teste;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Amazonteste {

	private WebDriver driver;
	
	@Before 
	public void abrir () {
		
		System.setProperty("webdriver.gecko.driver","C:/Driver/geckodriver.exe");
		
		driver = new FirefoxDriver(); 
		driver.manage().window().maximize();
		driver.get ("https://www.amazon.com.br/");
		
		
	}
	
	@After
	public void sair () {
		driver.quit();
		
		
	}
	
	@Test
	public void maisVendidos() throws InterruptedException {
		
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id=\"nav-xshop\"]/a[2]")).click();
		Thread.sleep(3000);
		Assert.assertEquals("Mais vendidos", driver.findElement(By.id("zg_banner_text")).getText());
		
		
		
	}
	
	
}
	
	
	
	
	
	
